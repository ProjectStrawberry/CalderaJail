package com.calderaminecraft.calderajail;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CalderaJail extends JavaPlugin {
    CalderaJail plugin;
    private File dbFile;
    private Connection conn = null;

    Map<String, Long> afkPlayers = new HashMap<>();
    Map<String, Long> jailedDuration = new HashMap<>();
    Map<String, Long> jailedServed = new HashMap<>();
    Map<String, UUID> uuidCache = new HashMap<>();
    List<String> jailIds = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        dbFile = new File(plugin.getDataFolder(), "CalderaJail.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: CalderaJail.db");
            }
        }

        try {
            Statement stmt;
            ResultSet rs;

            Connection c = plugin.getConnection();

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS jail (uuid TEXT, length INTEGER, timeServed INTEGER, reason TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER, yaw INTEGER, pitch INTEGER)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS cells (jailId TEXT, jailName TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER, yaw INTEGER, pitch INTEGER)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS uuidCache (playerName TEXT, uuid TEXT, updated INTEGER)");

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT count(*) FROM uuidCache");

            if (rs.getInt("count(*)") > 0) {
                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM uuidCache");

                while (rs.next()) {
                    plugin.uuidCache.put(rs.getString("playerName").toLowerCase(), UUID.fromString(rs.getString("uuid")));
                }
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM cells");

            while (rs.next()) {
                jailIds.add(rs.getString("jailId"));
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT count(*) FROM jail");

            if (rs.getInt("count(*)") < 1) {
                stmt.close();
            } else {
                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM jail");

                while (rs.next()) {
                    Player curPlayer = Bukkit.getPlayer(UUID.fromString(rs.getString("uuid")));
                    if (curPlayer == null) continue;
                    if (!curPlayer.isOnline()) continue;
                    jailedDuration.put(curPlayer.getUniqueId().toString(), rs.getLong("length"));
                    jailedServed.put(curPlayer.getUniqueId().toString(), rs.getLong("timeServed"));
                }
                stmt.close();
            }

        } catch (Exception e) {
            plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            plugin.getLogger().severe("CalderaJail failed to load.");
            return;
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("lockup").setExecutor(new CommandLockUp(this));

        BukkitTask task = new BukkitRunnable() {
            public void run() {
                try {
                    PreparedStatement pstmt;
                    Connection c = plugin.getConnection();

                    for (Map.Entry<String, Long> entry : jailedServed.entrySet()) {
                        String uuid = entry.getKey();
                        long timeServed = entry.getValue();
                        long length = jailedDuration.get(uuid);
                        Player curPlayer = Bukkit.getPlayer(UUID.fromString(uuid));
                        if (curPlayer == null) continue;
                        if (!curPlayer.isOnline()) continue;

                        if (!afkPlayers.containsKey(curPlayer.getUniqueId().toString())) continue;
                        long afkSince = (System.currentTimeMillis() - afkPlayers.get(curPlayer.getUniqueId().toString()));
                        if (afkSince > (plugin.getConfig().getInt("afktime") * (60 * 1000))) continue;

                        timeServed += (1 * 1000);
                        if (timeServed >= length && length > 0) {
                            pstmt = c.prepareStatement("DELETE FROM jail WHERE uuid=?");
                            pstmt.setString(1, uuid);
                            pstmt.executeUpdate();
                            pstmt.close();
                            curPlayer.sendMessage(ChatColor.GOLD + "You have served your jail sentence and have been released.");
                            World targetWorld = Bukkit.getWorld(plugin.getConfig().getString("respawnpoint.world"));
                            if (targetWorld == null) {
                                plugin.getLogger().warning("The current jail respawn point was set in a world that no longer exists.");
                                continue;
                            }
                            Location loc = new Location(targetWorld,
                                    plugin.getConfig().getDouble("respawnpoint.x"),
                                    plugin.getConfig().getDouble("respawnpoint.y"),
                                    plugin.getConfig().getDouble("respawnpoint.z"),
                                    (float) plugin.getConfig().getDouble("respawnpoint.yaw"),
                                    (float) plugin.getConfig().getDouble("respawnpoint.pitch"));
                            curPlayer.teleport(loc);
                            jailedDuration.remove(uuid);
                            jailedServed.remove(uuid);
                        } else {
                            jailedServed.replace(uuid, timeServed);
                        }
                    }
                } catch (Exception e) {
                    plugin.getLogger().severe("timer: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }.runTaskTimer(this, 0, (1 * 20));

        BukkitTask updateDb = new BukkitRunnable() {
            public void run() {
                try {
                    PreparedStatement pstmt = null;
                    Connection c = plugin.getConnection();

                    for (Map.Entry<String, Long> entry : jailedServed.entrySet()) {
                        String uuid = entry.getKey();
                        long timeServed = entry.getValue();

                        pstmt = c.prepareStatement("UPDATE jail SET timeServed=? WHERE uuid=?");
                        pstmt.setLong(1, timeServed);
                        pstmt.setString(2, uuid);
                        pstmt.executeUpdate();
                    }

                    if (pstmt != null) pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().severe("updateDb timer: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }.runTaskTimer(this, 0, (5 * 60 * 20));

        plugin.getLogger().info("CalderaJail loaded.");
    }

    @Override
    public void onDisable() {
        try {
            plugin.getConnection().close();
        } catch (Exception e) {
            getLogger().warning("Error when disabling CalderaCodes: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

    Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.dbFile);
        }
        return conn;
    }

    String convertTime(long ms) {
        long seconds = (ms / 1000) % 60;
        long minutes = (ms / (1000 * 60)) % 60;
        long hours = (ms / (1000 * 60 * 60)) % 24;
        long days = (ms / (1000 * 60 * 60 * 24));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    long stringToTime(String time) {
        String validModifiers = "smhd";
        char timeModifier = time.charAt(time.length() - 1);
        int timeNumber;
        long timeDuration = 0;
        if (validModifiers.indexOf(timeModifier) < 0) {
            return 0;
        }

        try {
            timeNumber = Integer.parseInt(time.substring(0, time.length() - 1));
        } catch (NumberFormatException e) {
            return 0;
        }


        if (timeModifier == 's') {
            timeDuration = timeNumber * 1000L;
        }
        if (timeModifier == 'm') {
            timeDuration = timeNumber * 60 * 1000L;
        }
        if (timeModifier == 'h') {
            timeDuration = timeNumber * 60 * 60 * 1000L;
        }
        if (timeModifier == 'd') {
            timeDuration = timeNumber * 24 * 60 * 60 * 1000L;
        }

        return timeDuration;
    }
}
