package com.calderaminecraft.calderajail;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class EventListeners implements Listener {
    private CalderaJail plugin;

    EventListeners(CalderaJail p) {
        plugin = p;
    }

    @EventHandler
    public void AsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        plugin.afkPlayers.remove(player.getUniqueId().toString());
        plugin.afkPlayers.put(player.getUniqueId().toString(), System.currentTimeMillis());
    }
    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        plugin.afkPlayers.remove(player.getUniqueId().toString());
        plugin.afkPlayers.put(player.getUniqueId().toString(), System.currentTimeMillis());
    }
    @EventHandler
    public void PlayerInteractEntityEvent(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        plugin.afkPlayers.remove(player.getUniqueId().toString());
        plugin.afkPlayers.put(player.getUniqueId().toString(), System.currentTimeMillis());
    }
    @EventHandler
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        plugin.afkPlayers.remove(player.getUniqueId().toString());
        plugin.afkPlayers.put(player.getUniqueId().toString(), System.currentTimeMillis());
    }
    @EventHandler (priority = EventPriority.HIGHEST)
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        try {
            PreparedStatement pstmt;
            ResultSet rs;

            Connection c = plugin.getConnection();

            pstmt = c.prepareStatement("SELECT count(*) FROM uuidCache WHERE uuid=? AND playerName != ?");
            pstmt.setString(1, player.getUniqueId().toString());
            pstmt.setString(2, player.getName());
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt = c.prepareStatement("INSERT INTO uuidCache (playerName, uuid, updated) VALUES (?, ?, ?)");
                pstmt.setString(1, player.getName());
                pstmt.setString(2, player.getUniqueId().toString());
                pstmt.setLong(3, System.currentTimeMillis());
                pstmt.executeUpdate();

                plugin.uuidCache.put(player.getName().toLowerCase(), player.getUniqueId());
            } else {

                pstmt = c.prepareStatement("SELECT * FROM uuidCache WHERE uuid=?");
                pstmt.setString(1, player.getUniqueId().toString());
                rs = pstmt.executeQuery();

                String oldName = rs.getString("playerName");

                pstmt = c.prepareStatement("UPDATE uuidCache SET playerName=?, updated=? WHERE uuid=?");
                pstmt.setString(1, player.getName());
                pstmt.setLong(2, System.currentTimeMillis());
                pstmt.setString(3, player.getUniqueId().toString());
                pstmt.executeUpdate();

                plugin.uuidCache.remove(oldName);
                plugin.uuidCache.put(player.getName(), player.getUniqueId());
            }

            pstmt = c.prepareStatement("SELECT count(*) FROM jail WHERE uuid=?");
            pstmt.setString(1, player.getUniqueId().toString());
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt.close();
                return;
            }

            pstmt = c.prepareStatement("SELECT * FROM jail WHERE uuid=?");
            pstmt.setString(1, player.getUniqueId().toString());
            rs = pstmt.executeQuery();

            long timeMs = rs.getLong("length");
            long timeServedMs = rs.getLong("timeServed");
            String reason = rs.getString("reason");

            plugin.jailedDuration.put(player.getUniqueId().toString(), timeMs);
            plugin.jailedServed.put(player.getUniqueId().toString(), timeServedMs);

            player.sendMessage(ChatColor.GREEN + "Jail Information:");
            player.sendMessage(ChatColor.GREEN + "You are currently in jail " + (timeMs > 0 ? "for " + ChatColor.GOLD + plugin.convertTime(timeMs) : "indefinitely."));
            player.sendMessage(ChatColor.GREEN + "You have served " + ChatColor.GOLD + plugin.convertTime(timeServedMs) + ChatColor.GREEN + " of your sentence so far.");
            player.sendMessage(ChatColor.GREEN + "You were jailed for " + ChatColor.GOLD + reason);
            player.sendMessage(ChatColor.GREEN + "You must be online and not afk to serve out your sentence.");

            World targetWorld = Bukkit.getWorld(rs.getString("world"));

            if (targetWorld == null) {
                plugin.plugin.getLogger().warning("Error: The world " + rs.getString("world") + " no longer exists.");
                pstmt.close();
                return;
            }

            Location jailLocation = new Location(targetWorld, rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("yaw"), rs.getFloat("pitch"));
            player.teleport(jailLocation);

            pstmt.close();
        } catch (Exception e) {
            plugin.plugin.getLogger().severe("PlayerJoinEvent: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }
    @EventHandler
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (player == null) return;
        if (plugin.jailedDuration.containsKey(player.getUniqueId().toString())) {
            try {
                PreparedStatement pstmt;

                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("UPDATE jail SET timeServed=? WHERE uuid=?");
                pstmt.setLong(1, plugin.jailedServed.get(player.getUniqueId().toString()));
                pstmt.setString(2, player.getUniqueId().toString());
                pstmt.executeUpdate();
                pstmt.close();

            } catch (Exception e) {
                plugin.plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                plugin.plugin.getLogger().severe("PlayerQuitEvent database error.");
                return;
            }
            plugin.jailedDuration.remove(player.getUniqueId().toString());
            plugin.jailedServed.remove(player.getUniqueId().toString());
        }
    }
    @EventHandler
    public void PlayerKickEvent(PlayerKickEvent event) {
        Player player = event.getPlayer();
        if (player == null) return;
        if (plugin.jailedDuration.containsKey(player.getUniqueId().toString())) {
            try {
                PreparedStatement pstmt;

                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("UPDATE jail SET timeServed=? WHERE uuid=?");
                pstmt.setLong(1, plugin.jailedServed.get(player.getUniqueId().toString()));
                pstmt.setString(2, player.getUniqueId().toString());
                pstmt.executeUpdate();
                pstmt.close();

            } catch (Exception e) {
                plugin.plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                plugin.plugin.getLogger().severe("PlayerQuitEvent database error.");
                return;
            }
            plugin.jailedDuration.remove(player.getUniqueId().toString());
            plugin.jailedServed.remove(player.getUniqueId().toString());
        }
    }
    @EventHandler
    public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();

        plugin.afkPlayers.remove(player.getUniqueId().toString());
        plugin.afkPlayers.put(player.getUniqueId().toString(), System.currentTimeMillis());

        String command;
        if (message.contains(" ")) {
            command = message.substring(1, message.indexOf(" "));
        } else {
            command = message.substring(1);
        }

        try {
            PreparedStatement pstmt;
            ResultSet rs;

            Connection c = plugin.getConnection();

            pstmt = c.prepareStatement("SELECT count(*) FROM jail WHERE uuid=?");
            pstmt.setString(1, player.getUniqueId().toString());
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt.close();
            } else {
                pstmt.close();

                if (!plugin.plugin.getConfig().getStringList("commands").contains(command)) {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You may not use this command while in jail.");
                }
            }

        } catch (Exception e) {
            plugin.plugin.getLogger().severe("PlayerCommandPreprocessEvent: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }
}
