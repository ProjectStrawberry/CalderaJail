package com.calderaminecraft.calderajail;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CommandLockUp implements TabExecutor {
    private CalderaJail plugin;

    CommandLockUp(CalderaJail p) {
        plugin = p;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length < 1) {
            boolean hasPermission = false;
            if (sender.hasPermission("lockup.use")) {
                hasPermission = true;
                sender.sendMessage(ChatColor.GREEN + "CalderaJail User Commands:");
                sender.sendMessage(ChatColor.GOLD + "/lockup time - Displays how much time must be served.");
            }
            if (sender.hasPermission("lockup.admin")) {
                hasPermission = true;
                sender.sendMessage(ChatColor.GREEN + "CalderaJail Admin Commands:");
                sender.sendMessage(ChatColor.GOLD + "/lockup <player> <jail name> <time> <reason> - Sends a player to jail.");
                sender.sendMessage(ChatColor.GOLD + "/lockup parole <player> - Releases a player from jail.");
                sender.sendMessage(ChatColor.GOLD + "/lockup setjail <jail name> - Creates a new jail location.");
                sender.sendMessage(ChatColor.GOLD + "/lockup deljail <jail name> - Deletes a jail.");
                sender.sendMessage(ChatColor.GOLD + "/lockup setrespawn - Sets the location prisoners spawn at when released from jail.");
                sender.sendMessage(ChatColor.GOLD + "/lockup time <player> - Displays how much time a player needs to serve.");
            }
            if (!hasPermission) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            }
            return true;
        }

        if (args[0].equals("time")) {
            Player targetPlayer;
            OfflinePlayer offlinePlayer = null;
            boolean target = false;
            if (args.length > 1) {
                if (!sender.hasPermission("lockup.admin")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                targetPlayer = Bukkit.getPlayer(args[1]);
                target = true;
            } else {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /lockup time <player> - Displays how long a player is in prison for.");
                    return true;
                }
                targetPlayer = (Player) sender;
            }

            if (targetPlayer == null && !plugin.uuidCache.containsKey(args[1].toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                return true;
            } else if (targetPlayer == null) {
                offlinePlayer = Bukkit.getOfflinePlayer(plugin.uuidCache.get(args[1].toLowerCase()));
            }

            try {
                PreparedStatement pstmt;
                ResultSet rs;

                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("SELECT count(*) FROM jail WHERE uuid=?");
                pstmt.setString(1, (targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + (target ? "That user is " : "You are ") + "not currently in jail.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM jail WHERE uuid=?");
                pstmt.setString(1, (targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
                rs = pstmt.executeQuery();

                long timeMs = rs.getLong("length");
                long timeServedMs = rs.getLong("timeServed");

                if (plugin.jailedServed.containsKey((targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString())) {
                    timeServedMs = plugin.jailedServed.get((targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
                }

                String reason = rs.getString("reason");

                if (target) {
                    sender.sendMessage(ChatColor.GREEN + "Jail Information for " + (targetPlayer == null ? offlinePlayer.getName() : targetPlayer.getDisplayName()));
                    sender.sendMessage(ChatColor.GREEN + "Sentence: " + ChatColor.GOLD + (timeMs > 0 ? plugin.convertTime(timeMs) : "Indefinite"));
                    sender.sendMessage(ChatColor.GREEN + "Time Served: " + ChatColor.GOLD + plugin.convertTime(timeServedMs));
                    sender.sendMessage(ChatColor.GREEN + "Reason: " + ChatColor.GOLD + reason);
                } else {
                    sender.sendMessage(ChatColor.GREEN + "Jail Information:");
                    sender.sendMessage(ChatColor.GREEN + "You are currently in jail " + (timeMs > 0 ? "for " + ChatColor.GOLD + plugin.convertTime(timeMs) : "indefinitely."));
                    sender.sendMessage(ChatColor.GREEN + "You have served " + ChatColor.GOLD + plugin.convertTime(timeServedMs) + ChatColor.GREEN + " of your sentence so far.");
                    sender.sendMessage(ChatColor.GREEN + "You were jailed for " + ChatColor.GOLD + reason);
                    sender.sendMessage(ChatColor.GREEN + "You must be online and not afk to serve out your sentence.");
                }

                pstmt.close();
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/lockup time: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (!sender.hasPermission("lockup.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args[0].equals("list")) {
            try {
                Statement stmt;
                ResultSet rs;

                Connection c = plugin.getConnection();

                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT count(*) FROM cells");

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no jails added.");
                    stmt.close();
                    return true;
                }

                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM cells");

                sender.sendMessage(ChatColor.DARK_GREEN + "CalderaJail jail names:");
                while (rs.next()) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "Name: " + ChatColor.GOLD + rs.getString("jailName") + ChatColor.DARK_GREEN + " Jail Location: " +
                            ChatColor.GOLD + "x: " + rs.getInt("x") + " y: " + rs.getInt("y") + " z: " + rs.getInt("z") + ChatColor.DARK_GREEN + " in world " +
                            ChatColor.GOLD + rs.getString("world"));
                }
                stmt.close();
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/lockup list: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args[0].equals("setrespawn")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            Player player = (Player) sender;
            Location loc = player.getLocation();

            plugin.getConfig().set("respawnpoint.world", player.getWorld().getName());
            plugin.getConfig().set("respawnpoint.x", loc.getX());
            plugin.getConfig().set("respawnpoint.y", loc.getY());
            plugin.getConfig().set("respawnpoint.z", loc.getZ());
            plugin.getConfig().set("respawnpoint.yaw", loc.getYaw());
            plugin.getConfig().set("respawnpoint.pitch", loc.getPitch());
            plugin.saveConfig();

            sender.sendMessage(ChatColor.GOLD + "The jail respawn point has been set.");
            return true;
        }

        if (args[0].equals("parole")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /lockup parole <player> - Releases a player from jail.");
                return true;
            }
            Player targetPlayer = Bukkit.getPlayer(args[1]);
            OfflinePlayer offlinePlayer = null;
            if (targetPlayer == null && !plugin.uuidCache.containsKey(args[1].toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                return true;
            } else if (targetPlayer == null) {
                offlinePlayer = Bukkit.getOfflinePlayer(plugin.uuidCache.get(args[1].toLowerCase()));
            }

            try {
                PreparedStatement pstmt;
                ResultSet rs;

                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("SELECT count(*) FROM jail WHERE uuid=?");
                pstmt.setString(1, (targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player is not currently in jail.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("DELETE FROM jail WHERE uuid=?");
                pstmt.setString(1, (targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
                pstmt.executeUpdate();
                pstmt.close();

                plugin.jailedDuration.remove((targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
                plugin.jailedServed.remove((targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());

                sender.sendMessage(ChatColor.GOLD + "You have released " + (targetPlayer == null ? offlinePlayer.getName() : targetPlayer.getDisplayName()) + ChatColor.GOLD + " from jail.");
                if (targetPlayer != null) {
                    targetPlayer.sendMessage(ChatColor.GOLD + "You have been released from jail early.");
                    World targetWorld = Bukkit.getWorld(plugin.getConfig().getString("respawnpoint.world"));
                    if (targetWorld == null) {
                        plugin.getLogger().warning("The current jail respawn point was set in a world that no longer exists.");
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The current jail respawn point was set in a world that no longer exists.");
                        return true;
                    }
                    Location loc = new Location(targetWorld,
                            plugin.getConfig().getDouble("respawnpoint.x"),
                            plugin.getConfig().getDouble("respawnpoint.y"),
                            plugin.getConfig().getDouble("respawnpoint.z"),
                            (float) plugin.getConfig().getDouble("respawnpoint.yaw"),
                            (float) plugin.getConfig().getDouble("respawnpoint.pitch"));
                    targetPlayer.teleport(loc);
                }
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/lockup parole: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args[0].equals("setjail")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /lockup setjail <jail name> - Creates a new jail.");
                return true;
            }
            Player player = (Player) sender;
            String jailName = args[1];
            String jailId = args[1].toLowerCase();

            try {
                PreparedStatement pstmt;
                ResultSet rs;

                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("SELECT count(*) FROM cells WHERE jailId=?");
                pstmt.setString(1, jailId);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") > 0) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A jail with that name already exists.");
                    pstmt.close();
                    return true;
                }

                Location loc = player.getLocation();

                pstmt = c.prepareStatement("INSERT INTO cells (jailId, jailName, world, x, y, z, yaw, pitch) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                pstmt.setString(1, jailId);
                pstmt.setString(2, jailName);
                pstmt.setString(3, player.getWorld().getName());
                pstmt.setDouble(4, loc.getX());
                pstmt.setDouble(5, loc.getY());
                pstmt.setDouble(6, loc.getZ());
                pstmt.setFloat(7, loc.getYaw());
                pstmt.setFloat(8, loc.getPitch());
                pstmt.executeUpdate();

                pstmt.close();
                plugin.jailIds.add(jailId);

                sender.sendMessage(ChatColor.GOLD + "You have created a new jail.");
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/lockup setjail: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args[0].equals("deljail")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /lockup deljail <jail name> - Deletes a jail.");
                return true;
            }
            String jailId = args[1].toLowerCase();

            try {
                PreparedStatement pstmt;
                ResultSet rs;

                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("SELECT count(*) FROM cells WHERE jailId=?");
                pstmt.setString(1, jailId);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A jail with that name does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("DELETE FROM cells WHERE jailId=?");
                pstmt.setString(1, jailId);
                pstmt.executeUpdate();

                pstmt.close();
                plugin.jailIds.remove(jailId);

                sender.sendMessage(ChatColor.GOLD + "You have deleted a jail.");
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/lockup deljail: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args.length < 4) {
            sender.sendMessage(ChatColor.GOLD + "Usage: /lockup <player> <jail name> <time> <reason> - Sends a player to jail.");
            return true;
        }

        if (plugin.getConfig().get("respawnpoint") == null) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must set a respawn point before you can jail players.");
            return true;
        }

        Player targetPlayer = Bukkit.getPlayer(args[0]);
        OfflinePlayer offlinePlayer = null;
        String jailId = args[1].toLowerCase();
        String time = args[2];
        String reason = "";

        if (targetPlayer == null && !plugin.uuidCache.containsKey(args[0].toLowerCase())) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player is not online.");
            return true;
        } else if (targetPlayer == null) {
            offlinePlayer = Bukkit.getOfflinePlayer(plugin.uuidCache.get(args[0].toLowerCase()));
        }

        for (int i=3; i<args.length; i++) reason += args[i] + " ";

        reason = reason.trim();
        long timeDuration = 0;
        if (!time.equals("forever")) {
            timeDuration = plugin.stringToTime(time);
        } else {
            timeDuration = -1;
        }

        if (timeDuration == 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid time format.");
            return true;
        }

        try {
            PreparedStatement pstmt;
            ResultSet rs;

            Connection c = plugin.getConnection();

            pstmt = c.prepareStatement("SELECT count(*) FROM cells WHERE jailId=?");
            pstmt.setString(1, jailId);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A jail with that name does not exist.");
                pstmt.close();
                return true;
            }

            pstmt = c.prepareStatement("SELECT * FROM cells WHERE jailId=?");
            pstmt.setString(1, jailId);
            rs = pstmt.executeQuery();

            World targetWorld = Bukkit.getWorld(rs.getString("world"));

            if (targetWorld == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The world " + ChatColor.RED + rs.getString("world") + ChatColor.DARK_RED + " no longer exists.");
                pstmt.close();
                return true;
            }

            Location jailLocation = new Location(targetWorld, rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("yaw"), rs.getFloat("pitch"));

            pstmt = c.prepareStatement("SELECT count(*) FROM jail WHERE uuid=?");
            pstmt.setString(1, (targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") > 0) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That user is already in jail.");
                pstmt.close();
                return true;
            }

            pstmt = c.prepareStatement("INSERT INTO jail (uuid, length, timeServed, reason, world, x, y, z, yaw, pitch) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, (targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString());
            pstmt.setLong(2, timeDuration);
            pstmt.setInt(3, 0);
            pstmt.setString(4, reason);
            pstmt.setString(5, targetWorld.getName());
            pstmt.setDouble(6, jailLocation.getX());
            pstmt.setDouble(7, jailLocation.getY());
            pstmt.setDouble(8, jailLocation.getZ());
            pstmt.setFloat(9, jailLocation.getYaw());
            pstmt.setFloat(10, jailLocation.getPitch());
            pstmt.executeUpdate();
            pstmt.close();

            plugin.jailedDuration.put((targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString(), timeDuration);
            plugin.jailedServed.put((targetPlayer == null ? offlinePlayer : targetPlayer).getUniqueId().toString(), (long) 0);

            if (targetPlayer != null) {
                targetPlayer.teleport(jailLocation);
                if (timeDuration > 0) {
                    targetPlayer.sendMessage(ChatColor.GOLD + "You have been sent to jail for " + ChatColor.RED + plugin.convertTime(timeDuration));
                } else {
                    targetPlayer.sendMessage(ChatColor.GOLD + "You have been sent to jail.");
                }
            }

            sender.sendMessage(ChatColor.GOLD + "You have sent " + (targetPlayer == null ? offlinePlayer.getName() : targetPlayer.getDisplayName()) + ChatColor.GOLD + " to jail for" + (timeDuration > 0 ? ChatColor.RED + plugin.convertTime(timeDuration) : "ever."));

            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/lockup: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();

        boolean isCommand = (args[0].equals("parole") || args[0].equals("setjail") || args[0].equals("deljail") || args[0].equals("setrespawn") || args[0].equals("time"));
        if (sender.hasPermission("lockup.admin")) {
            if (args.length < 2) return null;
            if (args.length == 2) {
                if (args[0].equals("setjail") || args[0].equals("deljail")) tabComplete.add("<jailname>");
                if (!isCommand) tabComplete.addAll(plugin.jailIds);
            }
            if (args.length == 3 && !isCommand) {
                tabComplete.add("<time>");
                tabComplete.add("forever");
            }
            if (args.length == 4 && !isCommand) tabComplete.add("<reason>");
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
